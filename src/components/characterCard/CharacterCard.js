import React from 'react';
import Styles from './CharacterCard.module.css'; 

function CharacterCard (props) {
  let {character} = props;

  return (
    <div className={Styles.CharacterCard} ref={props.cardRef} onMouseOver={props.onMouseOver} onMouseOut={props.onMouseOut}>
      <div className={`card ${Styles.card}`}>
        <img className={`card-img ${Styles.cardImg}`} src={character.image} alt={character.name} />
        <div className="card-img-overlay">
          <h5 className={`card-title ${Styles.cardTitle}`}>{character.name}</h5>
        </div>
      </div>
    </div>
  );
}

export default CharacterCard;