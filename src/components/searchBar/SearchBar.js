import React from 'react';
import Styles from './SearchBar.module.css'; 

function SearchBar(props) {

  const handleChange = () => {
    props.search();
  }

  return (
    <form className={Styles.SearchBar}>
      <input 
        ref={props.searchRef} 
        onChange={handleChange} 
        type="text" id="search" placeholder="Search for character" 
        className={Styles.input}
      />
    </form>
  );
}

export default SearchBar;