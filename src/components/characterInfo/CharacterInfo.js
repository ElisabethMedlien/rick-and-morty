import React from 'react';
import Styles from './CharacterInfo.module.css'; 

function CharacterInfo (props) {
  let {character} = props;

  return (
    <div className={`p-2 ${Styles.CharacterInfo}`}>
        <h2>{character.name}</h2>
        <img src={character.image} width="150" alt={character.name} />
        <ul className={`p-2 ${Styles.ul}`}>
            <li><strong>Status: </strong>{character.status}</li>
            <li><strong>Species: </strong>{character.species}</li>
            <li><strong>Gender: </strong>{character.gender}</li>
            {(character.type)? <li><strong>Type: </strong>{character.type}</li>: null}
        </ul>
  
    </div>
  );
}

export default CharacterInfo;