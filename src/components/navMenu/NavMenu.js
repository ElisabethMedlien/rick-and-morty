import React from 'react';
import img from '../../assets/brand.png';
import Styles from './NavMenu.module.css'; 

function NavMenu() {
  return (
    <nav className={`navbar ${Styles.navbar}`}>
      <a className={`navbar-brand ${Styles.center}`} href="/">    
        <img src={img} width="175" height="75" className="d-inline-block" alt="logo" /> 
      </a>
    </nav>
  );
}


/*<nav>
        <ul>
            <li>
                <Link to="/">Home</Link>
            </li>
        </ul>
    </nav> */

export default NavMenu;

