import React from 'react';
import './App.css';
import NavMenu from './components/navMenu/NavMenu.js';
import Main from './views/main/Main.js';
import Profile from './views/profile/Profile.js';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Router>
      <div className="App">
        <NavMenu />
        <Switch>
          <Route path="/" exact component={Main} />
          <Route path="/profile/:id" component={Profile} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
