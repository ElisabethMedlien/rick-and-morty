import React, {useState, useEffect} from 'react';
import CharacterInfo from '../../components/characterInfo/CharacterInfo.js';

function Profile (props) {
  const [character, setCharacter ] = useState([]);

  useEffect(() => {
    
    let {id} = props.match.params;
    fetch(`https://rickandmortyapi.com/api/character/${id}`)
    .then(resp => resp.json())
    .then(resp=> {
       setCharacter(resp);
    })
    .catch(err => console.error(err));
    // eslint-disable-next-line 
  }, []);

  return (
    <div className="Profile container">
      <div className="row my-4">
        <div className="col-sm-12 col-md-4 offset-md-4 text-center">
          <CharacterInfo character={character} />
        </div>
      </div>
    </div>
  );
}

export default Profile;