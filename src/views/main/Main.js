import React, {useState, useEffect, useRef} from 'react';
import CharacterCard from '../../components/characterCard/CharacterCard.js';
import SearchBar from '../../components/searchBar/SearchBar.js';
import Styles from './Main.module.css'; 

const Main = () => {

  const [nCharsToShow, setNCharactersToShow ] = useState(20);
  const [filteredCharacters, setFilteredCharacters ] = useState([]);
  const [characters, setCharacters ] = useState([]);
  const [pageNumber, setPageNumber ] = useState(1);
  const [totalPages, setTotalPages ] = useState(1);
  const getSearchRef = useRef(null);

  // add scroll event
  useEffect(() => {

    window.addEventListener('scroll', handleScroll);
      return () => {
        window.removeEventListener('scroll', handleScroll);
      }

    }, []);

  // render 20 more characters when scrolled to bottom 
  function handleScroll ()  {

    if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight) return;
     setNCharactersToShow(n => n + 20);
    }

  useEffect(() => {

    if (pageNumber <= totalPages) {

      let char = [...characters];

      fetch(`https://rickandmortyapi.com/api/character/?page=${pageNumber}`)
        .then(resp => resp.json())
        .then(resp => {

            char = [...char, ...resp.results];
            setCharacters(char);
            setTotalPages(resp.info.pages);
            
          })
          .then(() => 
               setPageNumber(pageNumber + 1)
          )
        .catch(err => console.error(err));
      } 
  }, 
  // eslint-disable-next-line 
  [pageNumber]);      

  const getFilteredCharacters = filteredCharacters.map(character => {
    return (
      <div className={`col-sm-12 col-md-4 col-lg-2 ${Styles.cardContainer}`} key={character.id}>  
        <a href={`/profile/${character.id}`} key={character.id}>
          <CharacterCard character={character} />
        </a>
      </div>
    );
  });

  const renderCharacters = () => {
    let char = [...characters];
    char = char.filter (character => character.id < nCharsToShow)
    return (
      char.map(character => {
        return (
          <div className={`col-sm-12 col-md-2 ${Styles.cardContainer}`} key={character.id}>
            <a href={`/profile/${character.id}`}>
              <CharacterCard character={character} />
            </a>
          </div>
        )
      })
    )
  }

  return (
    <div className="Main"> 
      <div className="container">

        <div className="row">
          <div className="col-sm-12">
            <SearchBar search={handleSearch} searchRef={getSearchRef} />
          </div>
        </div>
          
        <div className={`row ${Styles.cards}`}>

          { (getFilteredCharacters.length !== 0  ) ? 
              (<React.Fragment>{ getFilteredCharacters }</React.Fragment> ) 
            : 
              ( <React.Fragment>{ renderCharacters() }</React.Fragment> )
          }

        </div>
      </div>
    </div>
  );

  function handleSearch() {
    
    let char = [];

    characters.filter(character => {

      let ref = getSearchRef.current.value.toLowerCase();
      let name = character.name.toLowerCase();

      if (name.includes(ref)) {

        char.push(character);

      } 
      return char;
    });
    setFilteredCharacters(char);
  }
}

export default Main;